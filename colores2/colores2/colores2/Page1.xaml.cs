﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace colores2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        async private void ButtonClickedPrevios(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            


            if (sender == id_sl_width)
            {
                id_bv_backgorund.WidthRequest = (int)id_sl_width.Value;
                id_bv_backgorund.HeightRequest = (int)id_sl_width.Value;

                id_sl_hight.Value = id_sl_width.Value;

                id_lb_width.Text = id_sl_width.Value.ToString();
                id_lb_hight.Text = id_sl_width.Value.ToString();
            }
            if (sender == id_sl_hight)
            {
                id_bv_backgorund.WidthRequest = (int)id_sl_hight.Value;
                id_bv_backgorund.HeightRequest = (int)id_sl_hight.Value;

                id_sl_width.Value = id_sl_hight.Value;

                id_lb_width.Text = id_sl_hight.Value.ToString();
                id_lb_hight.Text = id_sl_hight.Value.ToString();
            }
        }

    }
}